﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Team 
{
    private string teamName;
    private string managerName;
    private List<Player> players;
    private Sprite kit;

    public Team(string teamName, string manager, string kit)
    {
        this.teamName = teamName;
        this.managerName = manager;
        players = new List<Player>();
        this.kit = Resources.Load<Sprite>(kit);
    }

    public string GetTeamName(){
        return teamName;
    }

    public string GetManagerName() {
        return managerName;
    }

    public void AddPlayerToTeam(Player player){
        if (players.Contains(player)==false)
        {
            players.Add(player);
        }
    }

    public Player GetPlayerFromTeam(int whichOne)
    {
        return players[whichOne];
    }

    public List<Player> GetFullTeam() {
        return players;
    }

    public Sprite GetKitSprite() {
        return kit;
    }

}
