﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerPosition{
    Goalkeeper,
    Defender,
    Midfield,
    Forward
}

public class Player 
{
    private string playerName;
    private PlayerPosition playerPosition;
    private string playerNumber;
    private bool isCaptain;

    public Player(string playerName, string playerNumber, string playerPosition, string isCaptain)
    {
        this.playerName = playerName;
        this.playerNumber = playerNumber;

        switch(playerPosition){
            case "GK":
                this.playerPosition = PlayerPosition.Goalkeeper;
                break;
            case "DF":
                this.playerPosition = PlayerPosition.Defender;
                break;
            case "MF":
                this.playerPosition = PlayerPosition.Midfield;
                break;
            case "FW":
                this.playerPosition = PlayerPosition.Forward;
                break;
        }

        if (isCaptain == "true")
        {
            this.isCaptain = true;
        }
        else this.isCaptain = false;
    }

    public string GetPlayerName(){
        return playerName;
    }

    public string GetPlayerNumber(){
        return playerNumber;
    }

    public PlayerPosition GetPlayerPosition() {
        return playerPosition;
    }
}
